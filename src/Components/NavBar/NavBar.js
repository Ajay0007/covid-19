import React, { useState,useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import Covidquotes from '../CovidQuotes/Covidquotes.js';
import LanguageSharpIcon from '@material-ui/icons/LanguageSharp';
import GlobalStat from '../../Components/GlobalStats/GlobalStat.js';
import PublicIcon from '@material-ui/icons/Public';
import WorldMap from '../../Components/WorldMap/Worldmap.js';
import DailyCovidNews from '../../Components/IndiaCovidNews/DaliyCovidNews.js';

const useStyles = makeStyles((theme) => ({
  navbar:{
    boxShadow: '1px 0px 1px black !important'
  },
  globalstat:{
    marginTop: '12px',
    cursor: 'pointer'
  },
  globalmap:{
    marginTop: '12px',
    cursor: 'pointer',
    marginRight: '14px'
  },
  grow: {
    flexGrow: 1,
  },
  title: {
    fontFamily: 'archia !important',
    display: 'block',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
    color: theme.palette.text.inherit,
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));
const PrimarySearchAppBar =  () => {
  const classes = useStyles();
  const [NewsCount, setNewsCount] = useState(0);
  const [isCountryModalVisible, setModalVisible] = useState(false);
  const [isMapModalVisible, setMapModalVisible] = useState(false);
  const [isNewsModalVisible, setNewsModalVisible] = useState(false);

  useEffect(() =>{
    console.log('News covid')
    var myHeaders = new Headers();
    myHeaders.append("Subscription-Key", "3009d4ccc29e4808af1ccc25c69b4d5d");
    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };
    fetch("https://api.smartable.ai/coronavirus/news/IN", requestOptions)
    .then(response => response.json())
    .then((data) =>{
      setNewsCount(data.news.length)
    })
    .catch(error => console.log('error', error));
  },[])

  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };
  const handleMapVisible = () =>{
    if(isMapModalVisible){
      setMapModalVisible(false)
    }else{
      setMapModalVisible(true)
    }
  }
  const handleCountryRecords = () =>{
    if(isCountryModalVisible){
      setModalVisible(false)
    }else{
      setModalVisible(true)
    }
  }
  const handleCovidNews = () =>{
    if(isNewsModalVisible){
      setNewsModalVisible(false)
    }else{
      setNewsModalVisible(true)
    }
  }
  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="secondary">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="static" color="inherit" className={classes.navbar}>
        <Toolbar>
          <Typography className={classes.title}  variant="h6" noWrap>
            Covid-19
          </Typography>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            {/* <div onClick={()=>handleMapVisible()} className={classes.globalmap}  >
              <PublicIcon />
            </div> */}
            <div onClick={()=>handleCountryRecords()} className={classes.globalstat}  >
              <LanguageSharpIcon />
            </div>
            <div onClick={()=>handleCovidNews()}>
              <IconButton aria-label="show new notifications" color="inherit">
                <Badge badgeContent={NewsCount} color="secondary">
                  <NotificationsIcon />
                </Badge>
              </IconButton>
            </div>
          </div>
          <div className={classes.sectionMobile}>
          {/* <div onClick={()=>handleMapVisible()} className={classes.globalmap}>
              <PublicIcon />
            </div> */}
            <div onClick={()=>handleCountryRecords()} className={classes.globalstat}  >
              <LanguageSharpIcon />
            </div>
            <div onClick={()=>handleCovidNews()}>
            <IconButton aria-label="show 11 new notifications" color="inherit">
              <Badge badgeContent={11} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
          </div>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <DailyCovidNews isNewsModalVisible={isNewsModalVisible} />
      <WorldMap isMapModalVisible={isMapModalVisible} />
      <GlobalStat isCountryModalVisible={isCountryModalVisible} />
      <Covidquotes />
      {renderMobileMenu}
    </div>
  );
}

export default PrimarySearchAppBar;








