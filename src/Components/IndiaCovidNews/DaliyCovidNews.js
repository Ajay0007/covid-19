import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import './DailyCovidNews.css';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import { FixedSizeList } from 'react-window';

class DailyCovidNews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModelVisible: false,
            DailyCovid19News:[]
          }
    }
    componentDidMount(){
        this.getCovidNews()
    }
    componentWillReceiveProps(nextProp){
        if (nextProp.isNewsModalVisible) {
            this.setState({ isModelVisible: nextProp.isNewsModalVisible })
        } else {
            this.setState({ isModelVisible: nextProp.isNewsModalVisible })
        }
    }
    getCovidNews(){
        var myHeaders = new Headers();
        myHeaders.append("Subscription-Key", "3009d4ccc29e4808af1ccc25c69b4d5d");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://api.smartable.ai/coronavirus/news/IN", requestOptions)
        .then(response => response.json())
        .then((data) =>{
            this.setState({ DailyCovid19News: data.news})
        })
        .catch(error => console.log('error', error));

    }
    render() { 
        return ( 
            <React.Fragment>
                <div className={(this.state.isModelVisible) ? 'CountriesModal ShowModal' : 'CountriesModal HideModal'}>
                    <Grid container 
                    justify="flex-end"
                    alignItems="center"
                    >
                        <Grid item xs={12} sm={12} md={5} lg={11} xl={11} >
                            <Card variant="outlined" style={{'marginRight': '45px'}}>
                                <CardContent>
                                    <Typography className='cardheading' variant="subtitle2" gutterBottom color="textSecondary">
                                        Daily Covid19 News.
                                    </Typography>
                                    <Divider />
                                    <List className='root Newscard'  >
                                        {(this.state.DailyCovid19News.length > 0)?
                                            this.state.DailyCovid19News.map((News, index) =>(
                                                <span key={index} >
                                                <ListItem key={index} alignItems="flex-start">
                                                    <ListItemAvatar>
                                                        <Avatar alt="Remy Sharp" src="https://media1.s-nbcnews.com/j/newscms/2020_16/331…16p_23fe61bb653e3f82510a4d7face3a142.fit-560w.jpg" />
                                                    </ListItemAvatar>
                                                    <ListItemText className='NewsTitle'
                                                        primary={News.title}
                                                        secondary={
                                                            <React.Fragment>
                                                                <Typography
                                                                    component="span"
                                                                    variant="body2"
                                                                    className='inline'
                                                                    color="textPrimary"
                                                                >
                                                                    Excerpt
                                                                </Typography>
                                                                <span className='MainNews'>{' - '+News.excerpt}</span>
                                                                <span className='NewsSource'>Sourcses -</span>
                                                                <span><a href={News.ampWebUrl}>{(!!News.ampWebUrl)? News.ampWebUrl:'Not available'}</a></span>
                                                            </React.Fragment>
                                                        }
                                                        
                                                    />
                                                </ListItem>
                                                <Divider variant="inset" component="li" />
                                                </span>
                                            )) : <span>New not Available</span>
                                        }
                                    </List>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </div>
            </React.Fragment>
         );
    }
}
 
export default DailyCovidNews;


