import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import StateDetails from '../StateDetailsSlider/StateDetails.js';
import CountUp from 'react-countup';
import Tabs from '../TabPanel/Tabs.js';
import StatMap from '../StateMap/StateMap.js';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  confirm: {
    color: 'rgba(255,7,58,.6)',
    fontFamily: 'archia',
    fontWeight: '900',
    textTransform: 'uppercase',
    fontSize: '12px !important',
  },
  confirmChild1: {
    textAlign: 'center',
    alignItem: 'center',
    justifyContent: 'center',
    marginTop: '.5rem',
    color: 'rgba(255,7,58,.6)',
    fontFamily: 'archia',
    fontWeight: 900,
    fontSize: '12px !important',
    textTransform: 'uppercase'
  },
  confirmChild2: {
    color: '#ff073a',
    fontWeight: '600',
    textAlign: 'center',
    fontFamily: 'archia',
    fontSize: '24px !important',
    fontWeight: '900',
    textTransform: 'uppercase'

  },
  active: {
    color: 'rgba(0,123,255,.6)',
    fontFamily: 'archia',
    fontWeight: '900',
    textTransform: 'uppercase',
    fontSize: '12px !important',
    [theme.breakpoints.up('xs')]: {
      marginLeft: '20px'
    }
  },
  activeChild1: {
    textAlign: 'center',
    alignItem: 'center',
    justifyContent: 'center',
    marginTop: '.5rem',
    color: '#ffffff',
    fontFamily: 'archia',
    fontWeight: 900,
    fontSize: '12px !important',
    textTransform: 'uppercase'
  },
  activeChild2: {
    color: '#007bff',
    fontWeight: '600',
    textAlign: 'center',
    fontFamily: 'archia',
    fontSize: '24px !important',
    fontWeight: '900',
    textTransform: 'uppercase'
  },
  recovered: {
    color: 'rgba(40,167,69,.6)',
    fontFamily: 'archia',
    fontWeight: '900',
    textTransform: 'uppercase',
    fontSize: '12px !important',
  },
  recoveredChild1: {
    textAlign: 'center',
    alignItem: 'center',
    justifyContent: 'center',
    marginTop: '.5rem',
    color: 'rgba(40,167,69,.6)',
    fontFamily: 'archia',
    fontWeight: 900,
    fontSize: '12px !important',
    textTransform: 'uppercase'
  },
  recoveredChild2: {
    color: '#28a745',
    fontWeight: '600',
    textAlign: 'center',
    fontFamily: 'archia',
    fontSize: '24px !important',
    fontWeight: '900',
    textTransform: 'uppercase'

  },
  deceased: {
    color: 'rgba(108,117,125,.6)',
    fontFamily: 'archia',
    fontWeight: '900',
    textTransform: 'uppercase',
    fontSize: '12px !important',
  },
  deceasedChild1: {
    textAlign: 'center',
    alignItem: 'center',
    justifyContent: 'center',
    marginTop: '.5rem',
    color: 'rgba(108,117,125,.6)',
    fontFamily: 'archia',
    fontWeight: 900,
    fontSize: '12px !important',
    textTransform: 'uppercase'
  },
  deceasedChild2: {
    color: '#6c757d',
    fontWeight: '600',
    textAlign: 'center',
    fontFamily: 'archia',
    fontSize: '24px !important',
    fontWeight: '900',
    textTransform: 'uppercase'

  },
  FirstGridRow: {
    marginTop: '40px !important'
  },
  SecondGridRow: {
    marginTop: '40px !important'
  },
  ThirdGridRow: {
    marginTop: '40px !important'
  },
  FourthGridRow: {
    marginTop: '0px',
    [theme.breakpoints.up('xs')]: {
      marginTop: '10px'
    }
  },
  statewisedata:{
      marginRight: '40px',
      textAlign: 'center',
      fontFamily: 'archia !important',
      fontWeight: '900',
      fontSize: '24px !important',
      textTransform: 'uppercase',
      color: '#343a40',
      [theme.breakpoints.up('xs')]: {
          marginTop: '20px'
      }
  },
  statechild:{
    color: 'rgba(108,117,125,.6)',
    fontSize: '12px !important',
    fontFamily: 'archia',
    fontWeight: '900',
    textTransform: 'uppercase',
    marginLeft: '37px'
  },
  statechild2:{
    color: '#4c75f2',
    fontSize: '12px',
    fontFamily: 'archia',
    fontWeight: '900',
    textTransform: 'uppercase',
    marginLeft: '37px',
    [theme.breakpoints.up('xs')]: {
      marginLeft: '0px',
      fontSize:'10px'
  }
  },
  covidsymbol:{
    fontSize: '10px'
  },
  highsysmbol:{
    padding: '0px 6px 0px 6px',
    background: '#FF9999',
    marginRight: '6px'
  },
  avgsysmbol:{
    padding: '0px 6px 0px 6px',
    background: '#FCC88C',
    marginRight: '6px'
  },
  lowsysmbol:{
    padding: '0px 6px 0px 6px',
    background: '#B1DC88',
    marginRight: '6px'
  },
  stateTotal:{
    color: '#46A4D1'
  }
}));

const CountryStat = () => {
  const classes = useStyles();
  const [countryStat, setCountryStat] = useState(0);
  const [ConfirmCount, setConfirmCount] = useState(0);
  const [ActivityCount, setActivityCount] = useState(0);
  const [RecoveredCount, setRecoveredCount] = useState(0);
  const [DeceasedCount, setDeceasedCount] = useState(0);

  useEffect(() => {
    fetch('https://api.covid19india.org/data.json')
      .then(res => res.json())
      .then((stat) =>{
          setCountryStat(stat.statewise[0]);
          setConfirmCount((function () {
            let confirm = parseInt(stat.statewise[0].confirmed);
              return confirm
              // return stat.statewise[0].confirmed.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
          })())
          setActivityCount((function () {
            let active = parseInt(stat.statewise[0].active);
              return active
          })())
          setRecoveredCount((function () {
            let recovered = parseInt(stat.statewise[0].recovered);
              return recovered
          })())
          setDeceasedCount((function () {
            let deaths = parseInt(stat.statewise[0].deaths);
              return deaths
          })())
      })
  }, []);
  return (
    <React.Fragment>
      <Grid container
        justify="space-evenly"
        alignItems="center"
        className={classes.FirstGridRow}
      >
        <Grid item>
          <Typography variant="subtitle1" className={classes.confirm} gutterBottom>
            Confirmed
          </Typography>
          <div>
            <Typography variant="subtitle1" className={classes.confirmChild1} gutterBottom>
              {'[' + '+' + countryStat.deltaconfirmed + ']'}
            </Typography>
          </div>
          <div>
            <Typography variant="subtitle1" className={classes.confirmChild2} gutterBottom>
             <CountUp end={ConfirmCount} duration={3.75} separator=","></CountUp> 
            </Typography>
          </div>
        </Grid>
        <Grid item>
          <Typography variant="subtitle1" className={classes.active} gutterBottom>
            Active
                </Typography>
          <div>
            <Typography variant="subtitle1" className={classes.activeChild1} gutterBottom>
              0
                    </Typography>
          </div>
          <div>
            <Typography variant="subtitle1" className={classes.activeChild2} gutterBottom>
              <CountUp end={ActivityCount} duration={3.75} separator=","></CountUp> 
            </Typography>
          </div>
        </Grid>
      </Grid>

      <Grid container
        justify="space-evenly"
        alignItems="center"
        className={classes.SecondGridRow}
      >
        <Grid item>
          <Typography variant="subtitle1" className={classes.recovered} gutterBottom>
            Recovered
                </Typography>
          <div>
            <Typography variant="subtitle1" className={classes.recoveredChild1} gutterBottom>
              {'[' + '+' + countryStat.deltarecovered + ']'}
            </Typography>
          </div>
          <div>
            <Typography variant="subtitle1" className={classes.recoveredChild2} gutterBottom>
              <CountUp end={RecoveredCount} duration={3.75} separator=","></CountUp> 
            </Typography>
          </div>
        </Grid>
        <Grid item>
          <Typography variant="subtitle1" className={classes.deceased} gutterBottom>
            Deceased
                </Typography>
          <div>
            <Typography variant="subtitle1" className={classes.deceasedChild1} gutterBottom>
              {'[' + '+' + countryStat.deltadeaths + ']'}
            </Typography>
          </div>
          <div>
            <Typography variant="subtitle1" className={classes.deceasedChild2} gutterBottom>
              <CountUp end={DeceasedCount} duration={3.75} separator=","></CountUp> 
            </Typography>
          </div>
        </Grid>
      </Grid>
      <Grid container
      className={classes.FourthGridRow}
      >
        <Grid item xs={12} sm={12} md={8} lg={12} xl={12}>  
        <Typography variant="subtitle1" className={classes.statewisedata}  gutterBottom>
            <div>State Wise Data</div>
            <div className={classes.statechild}>COMPILED FROM STATE GOVT. NUMBERS
            {/* <div className={classes.statechild2}>CLICK ON ANY BELOW STATE TO KNOW MORE.</div> */}
            </div>
        </Typography>
        </Grid>
      </Grid>
      <Grid container className={classes.ThirdGridRow}>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
          <Tabs />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default CountryStat;