import React, { Component } from 'react';
import './StateDetails.css';
import $ from 'jquery';
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper';
import _ from 'lodash';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CountUp from 'react-countup';


class StateDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dynamicClassname:'extraCls',
      sliderLog:'',
      clearhandleautoslide:'',
      StateWiseData:[],
      StateTotalCases:[]
    }
  }
  componentDidMount(){
    this.fetchStatesDetails();
  }
  fetchStatesDetails(){
    fetch('https://api.covid19india.org/state_district_wise.json')
    .then(res => res.json())
    .then(statewisedetails =>{ 
      this.setState({ StateWiseData : {...statewisedetails} })
      const statesTotalCount = [
        {
          state: '',
          Districts:{}
        }
      ];
      const stateTotalCases = [
        {
          state:'',
          TotalCases:0
        }
      ]
      let stateDistCountKerala = 0;
      let stateDistCountDelhi = 0;
      let stateDistCountTelangana = 0;
      let stateDistCountRajasthan = 0;
      let stateDistCountHaryana =0;
      let UttarPradesh =0;
      let Ladakh =0;
      let TamilNadu =0;
      let JammuandKashmir =0;
      let Karnataka =0;
      let Maharashtra =0;
      let Punjab =0;
      let AndhraPradesh =0;
      let Uttarakhand =0;
      let Odisha =0;
      let Puducherry =0;
      let WestBengal =0;
      let Chandigarh =0;
      let Gujarat =0;
      let HimachalPradesh =0;
      let MadhyaPradesh =0;
      let Bihar =0;
      let Manipur =0;
      let Mizoram =0;
      let Goa =0;
      let AndamanandNicobarIslands =0;
      let Jharkhand =0;
      let Assam =0;
      let ArunachalPradesh =0;
      let DadraandNagarHaveli =0;
      let Tripura =0;
      let Nagaland =0;
      let Meghalaya =0;

      for(let state in statewisedetails){
        statesTotalCount.push({state:state,Districts:statewisedetails[state].districtData})
      }
      for(let i=1; i<statesTotalCount.length; i++){
        for(let district in statesTotalCount[i].Districts){
          if(statesTotalCount[i].state == statesTotalCount[i].state){
              if(statesTotalCount[i].state == 'Kerala'){
                stateDistCountKerala += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Delhi'){
                stateDistCountDelhi += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Telangana'){
                stateDistCountTelangana += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Rajasthan'){
                stateDistCountRajasthan += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Haryana'){
                stateDistCountHaryana += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Uttar Pradesh'){
                UttarPradesh += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Ladakh'){
                Ladakh += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Tamil Nadu'){
                TamilNadu += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Jammu and Kashmir'){
                JammuandKashmir += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Karnataka'){
                Karnataka += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Maharashtra'){
                Maharashtra += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Punjab'){
                Punjab += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Andhra Pradesh'){
                AndhraPradesh += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Uttarakhand'){
                Uttarakhand += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Odisha'){
                Odisha += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Puducherry'){
                Puducherry += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'West Bengal'){
                WestBengal += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Chandigarh'){
                Chandigarh += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Gujarat'){
                Gujarat += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Himachal Pradesh'){
                HimachalPradesh += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Madhya Pradesh'){
                MadhyaPradesh += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Bihar'){
                Bihar += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Manipur'){
                Manipur += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Mizoram'){
                Mizoram += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Goa'){
                Goa += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Andaman and Nicobar Islands'){
                AndamanandNicobarIslands += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Jharkhand'){
                Jharkhand += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Assam'){
                Assam += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Arunachal Pradesh'){
                ArunachalPradesh += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Dadra and Nagar Haveli'){
                DadraandNagarHaveli += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Tripura'){
                Tripura += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Nagaland'){
                Nagaland += statesTotalCount[i].Districts[district].confirmed
              }else if(statesTotalCount[i].state == 'Meghalaya'){
                Meghalaya += statesTotalCount[i].Districts[district].confirmed
              }
          }
        }
      }

      const StateArray = [
        {state:'Kerala',totalcases:stateDistCountKerala},
        {state:'Delhi',totalcases:stateDistCountDelhi},
        {state:'Telangana',totalcases:stateDistCountTelangana},
        {state:'Rajasthan',totalcases:stateDistCountRajasthan},
        {state:'Haryana',totalcases:stateDistCountHaryana},
        {state:'UttarPradesh',totalcases:UttarPradesh},
        {state:'Ladakh',totalcases:Ladakh},
        {state:'TamilNadu',totalcases:TamilNadu},
        {state:'JammuandKashmir',totalcases:JammuandKashmir},
        {state:'Karnataka',totalcases:Karnataka},
        {state:'Maharashtra',totalcases:Maharashtra},
        {state:'Punjab',totalcases:Punjab},
        {state:'AndhraPradesh',totalcases:AndhraPradesh},
        {state:'Uttarakhand',totalcases:Uttarakhand},
        {state:'Odisha',totalcases:Odisha},
        {state:'Puducherry',totalcases:Puducherry},
        {state:'WestBengal',totalcases:WestBengal},
        {state:'Chandigarh',totalcases:Chandigarh},
        {state:'Gujarat',totalcases:Gujarat},
        {state:'HimachalPradesh',totalcases:HimachalPradesh},
        {state:'MadhyaPradesh',totalcases:MadhyaPradesh},
        {state:'Bihar',totalcases:Bihar},
        {state:'Manipur',totalcases:Manipur},
        {state:'Mizoram',totalcases:Mizoram},
        {state:'Goa',totalcases:Goa},
        {state:'AndamanandNicobarIslands',totalcases:AndamanandNicobarIslands},
        {state:'Jharkhand',totalcases:Jharkhand},
        {state:'Assam',totalcases:Assam},
        {state:'ArunachalPradesh',totalcases:ArunachalPradesh},
        {state:'DadraandNagarHaveli',totalcases:DadraandNagarHaveli},
        {state:'Tripura',totalcases:Tripura},
        {state:'Nagaland',totalcases:Nagaland},
        {state:'Meghalaya',totalcases:Meghalaya},
      ]
      const topStates = [
        {
        state:'',
        total:0
        }
      ];
      this.setState({ StateTotalCases: StateArray})
      this.state.StateTotalCases.map((item) =>{
         if(item.totalcases > 1000)
         {
          topStates.push({state:item.state,total:item.totalcases})
         }
      })
    })
  }

  render() {
    return (
      <React.Fragment>
        <Grid container
        justify="space-evenly"
        alignItems="center"
        >
          <Grid item md={11}>
            <Card variant="outlined">
              <CardContent>
              <div style={{'display':'grid'}}>
          <span style={{'width': '101%'}} className='covidsymbol'><span className='highsysmbol'></span>States above <big  className='stateTotal'>1000</big> covid cases.</span>
        </div>
        <div>
          <span className='covidsymbol'><span className='avgsysmbol'></span>States above <big className='stateTotal'>500</big> covid cases.</span>
        </div>
        <div>
          <span className='covidsymbol'><span className='lowsysmbol'></span>States below <big className='stateTotal'>100</big> covid cases.</span>
        </div>
                <Grid container  spacing={2}>

                  {
                    this.state.StateTotalCases.map((stateWiseDetails, index) =>{
                        return <Grid item xs={6} md={2} key={index}>
                                  <Card variant="outlined">
                                    <CardContent className={(stateWiseDetails.totalcases > 1000 )? 'highCases': (stateWiseDetails.totalcases < 100)? 'safeZone' : 'avrageCases'}>
                                      <div>
                                      <Typography  className='stateCards'>
                                        {stateWiseDetails.state}
                                      </Typography>
                                      </div>
                                      <div>
                                      <Typography  className='statetotal'>
                                      <CountUp end={stateWiseDetails.totalcases} duration={3.75} separator=","></CountUp> 
                                      </Typography>
                                      </div>
                                    </CardContent>
                                  </Card>
                                </Grid>
                    })
                  }
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

export default StateDetails;