import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import NewReleasesIcon from '@material-ui/icons/NewReleases';
import NetworkCellIcon from '@material-ui/icons/NetworkCell';
import SignalCellularAltIcon from '@material-ui/icons/SignalCellularAlt';
import StateDetails from '../StateDetailsSlider/StateDetails.js';
import StateChart from '../StateDetailsSlider/StateChart.js';
import './Tab.css';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScrollableTabsButtonForce() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [ishorizontal, setHorizontal] = React.useState(false);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          centered
          aria-label="scrollable force tabs example"
        >
          <Tab label="Demographic" icon={<NetworkCellIcon />} {...a11yProps(0)} />
          <Tab label="Statistical" icon={<SignalCellularAltIcon />} {...a11yProps(1)} />
          <Tab label="Comming Soon..." icon={<NewReleasesIcon />} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <StateChart />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <StateDetails />
      </TabPanel>
      <TabPanel value={value} index={2}>
        Comming Soon...
      </TabPanel>
    </div>
  );
}