import React, {useState, useEffect, useRef, useCallback} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import CountryStat from '../CountryStats/CountryStat.js';
import axios from 'axios';
import {formatDistance, format} from 'date-fns';

import {
    formatDate,
    formatDateAbsolute,
  } from '../../utils/commom-functions.js';

const useStyles = makeStyles((theme) => ({
    appUpdateDiv:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    appUpdateFont1:{
        fontFamily: 'archia !important',
        fontWeight: '900',
        textTransform: 'uppercase',
        color: '#343a40',
        [theme.breakpoints.up('xs')]: {
            marginTop: '20px'
          }
    },
    appUpdateFont2:{
        fontFamily: 'archia !important',
        fontWeight: '600',
        fontSize: '11px!important',
        textTransform: 'uppercase',
        color: '#6c757d'
    },
    appUpdateFont3:{
        fontFamily: 'archia !important',
        color: 'rgba(40,167,69,.6) !important',
        fontWeight: '900',
        fontSize: '12px !important',
        textTransform: 'uppercase',
        [theme.breakpoints.up('md')]: {
            marginLeft: '81px !important',
            marginTop: '40px !important'
          },
        [theme.breakpoints.up('xs')]: {
            marginTop: '5px',
            marginLeft: '25px'
          }
    },
    appUpdateFont4:{
        fontFamily: 'archia !important',
        color: 'rgb(40, 167, 69)',
        fontWeight: '900',
        fontSize: '14px !important',
        textTransform: 'uppercase',
        fontSize: '11px !important',
        [theme.breakpoints.up('md')]: {
            marginLeft: '74px !important',
          },
        [theme.breakpoints.up('xs')]: {
            marginLeft: '-4px',
            fontSize: '10px',
          }
    },
    appUpdateFont5:{
        fontFamily: 'archia !important',
        color: 'rgb(40, 167, 69)',
        fontWeight: '900',
        textTransform: 'uppercase',
        fontSize: '12px !important',
        [theme.breakpoints.up('md')]: {
            marginLeft: '68px !important'
          },
        [theme.breakpoints.up('xs')]: {
            marginLeft: '10px',
            fontSize: '10px',
          }
    }
}));

const ApplastUpdated = () => {
  const [states, setStates] = useState([]);
  const [fetched, setFetched] = useState(false);
  const [lastUpdated, setLastUpdated] = useState('');

  useEffect(() => {
    if (fetched === false) {
      getStates();
    }
  }, [fetched]);

  const getStates = async () => {
    try {
      const [
        response,
      ] = await Promise.all([
        axios.get('https://api.covid19india.org/data.json'),
      ]);
      setStates(response.data.statewise);
      setLastUpdated(response.data.statewise[0].lastupdatedtime);
      setFetched(true);
    } catch (err) {
      console.log(err);
    }
  };

    const classes = useStyles();
    return ( 
        <React.Fragment>
            <Grid container className={classes.appUpdateDiv}>
                <Grid item xs={7} sm={6} md={4} lg={6} xl={6}
                >
                    <Typography variant="h5" className={classes.appUpdateFont1}>
                        India COVID-19 Tracker
                    </Typography>
                    <Typography variant="h6" className={classes.appUpdateFont2}>
                        A Crowdsourced Initiative
                    </Typography>
                </Grid>
                <Grid item xs={4} sm={6} md={3} lg={6} xl={6}> 
                    <Typography variant="h5" className={classes.appUpdateFont3}>
                        Last Updated
                    </Typography>
                    <Typography variant="h6" className={classes.appUpdateFont4}>
                    {isNaN(Date.parse(formatDate(lastUpdated)))
                    ? ''
                    : formatDistance(
                        new Date(formatDate(lastUpdated)),
                        new Date()
                      ) + ' Ago'}
                    </Typography>
                    <Typography variant="h6" className={classes.appUpdateFont5}>
                    {isNaN(Date.parse(formatDate(lastUpdated)))
                    ? ''
                    : formatDateAbsolute(lastUpdated)}
                    </Typography>
                </Grid>
            </Grid>
            <CountryStat />
        </React.Fragment>
    );
}
 
export default ApplastUpdated;