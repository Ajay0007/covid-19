import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ApplastUpdated from '../ApplastUpdated/AppUpdated.js';

const useStyles = makeStyles((theme) => ({
    banner_parent:{
        background: 'rgba(32,26,162,.12549)',
        height: 'auto !important',
        marginTop: theme.spacing(3)
    },
    banner: {
        textAlign: 'center',
        margin: theme.spacing(3),
    },
    banner_text:{
        fontFamily: 'archia !important',
        fontSize: '.8rem !important',
        fontWeight: '600',
        color: 'rgba(32,26,162,.866667)'
    }
}));
const Covidquotes = (props) => {
    const classes = useStyles();
    const [Quotes, setQuotes] = useState('Stay Home, Stay Safe.');

    useEffect(() =>{
        setInterval(() =>{
            fetch('https://api.covid19india.org/website_data.json')
            .then(res => res.json())
            .then(quotes => {
                let randomNum = Math.abs(Math.floor(Math.random() * (1 - quotes.factoids.length) + 1));
                setQuotes(quotes.factoids[randomNum].banner)})
        },4000)
    },[]);
    const setNewQuote = () =>{
        fetch('https://api.covid19india.org/website_data.json')
            .then(res => res.json())
            .then(quotes => {
            let randomNum = Math.abs(Math.floor(Math.random() * (1 - quotes.factoids.length) + 1));
            setQuotes(quotes.factoids[randomNum].banner)
        })
    }
    return ( 
        <React.Fragment>
            <Grid container className={classes.banner_parent} onClick={() => setNewQuote()}>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} className={classes.banner}>
                    <Typography className={classes.banner_text}>
                       {Quotes}
                    </Typography>
                </Grid>
            </Grid>
            <ApplastUpdated />
        </React.Fragment>
     );
}

export default Covidquotes;