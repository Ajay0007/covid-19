import React, { useState,useEffect } from 'react';
import { ComposableMap, Geographies, Geography } from 'react-simple-maps';
import { scaleQuantile } from 'd3-scale';
import ReactTooltip from 'react-tooltip';

const INDIA_TOPO_JSON = require('./india.topo.json');

const PROJECTION_CONFIG = {
  scale: 350,
  center: [78.9629, 22.5937] 
};

// Red Variants
const COLOR_RANGE = [
  '#ffedea',
  '#ffcec5',
  '#ffad9f',
  '#ff8a75',
  '#ff5533',
  '#e2492d',
  '#be3d26',
  '#9a311f',
  '#782618'
];

const DEFAULT_COLOR = '#EEE';

const getRandomInt = () => {
  return parseInt(Math.random() * 100);
};

const geographyStyle = {
  default: {
    outline: 'none'
  },
  hover: {
    fill: '#ccc',
    transition: 'all 250ms',
    outline: 'none'
  },
  pressed: {
    outline: 'none'
  }
};

const getHeatMapData = () => {
  return [
    { id: 'AP', state: 'Andhra Pradesh', value: 0 },
    { id: 'AR', state: 'Arunachal Pradesh', value: 0 },
    { id: 'AS', state: 'Assam', value: 0 },
    { id: 'BR', state: 'Bihar', value: 0 },
    { id: 'CT', state: 'Chhattisgarh', value: 0 },
    { id: 'GA', state: 'Goa', value: 0 },
    { id: 'GJ', state: 'Gujarat', value: 0 },
    { id: 'HR', state: 'Haryana', value: 0 },
    { id: 'HP', state: 'Himachal Pradesh', value: 0 },
    { id: 'JH', state: 'Jharkhand', value: 0 },
    { id: 'KA', state: 'Karnataka', value: 0 },
    { id: 'KL', state: 'Kerala', value: 0 },
    { id: 'MP', state: 'Madhya Pradesh', value: 0 },
    { id: 'MH', state: 'Maharashtra', value: 0 },
    { id: 'MN', state: 'Manipur', value: 0 },
    { id: 'ML', state: 'Meghalaya', value: 0 },
    { id: 'MZ', state: 'Mizoram', value: 0 },
    { id: 'NL', state: 'Nagaland', value: 0 },
    { id: 'OR', state: 'Odisha', value: 0 },
    { id: 'PB', state: 'Punjab', value: 0 },
    { id: 'RJ', state: 'Rajasthan', value: 0 },
    { id: 'SK', state: 'Sikkim', value: 0 },
    { id: 'TN', state: 'Tamil Nadu', value: 0 },
    { id: 'TG', state: 'Telangana', value: 0 },
    { id: 'TR', state: 'Tripura', value: 0 },
    { id: 'UT', state: 'Uttarakhand', value: 0 },
    { id: 'UP', state: 'Uttar Pradesh', value: 0 },
    { id: 'WB', state: 'West Bengal', value: 0 },
    { id: 'WB', state: 'West Bengal', value: 0 },
    { id: 'AN', state: 'Andaman and Nicobar Islands', value: 0 },
    { id: 'CH', state: 'Chandigarh', value: 0 },
    { id: 'DN', state: 'Dadra and Nagar Haveli', value: 0 },
    { id: 'DD', state: 'Daman and Diu', value: 0 },
    { id: 'DL', state: 'Delhi', value: 0 },
    { id: 'JK', state: 'Jammu and Kashmir', value: 0 },
    { id: 'LA', state: 'Ladakh', value: 0 },
    { id: 'LD', state: 'Lakshadweep', value: 0 },
    { id: 'PY', state: 'Puducherry', value: 0 }
  ];
};

function App(props) {
  const [tooltipContent, setTooltipContent] = useState('');
  const [data, setData] = useState(getHeatMapData());

  useEffect(()=>{
      if(props.StateMapData.length !== 0){
    const getHeatMapData2 = () => {
        return [
          { id: 'AP', state: 'Andhra Pradesh', value: props.StateMapData[12].totalcases },
          { id: 'AR', state: 'Arunachal Pradesh', value: props.StateMapData[28].totalcases },
          { id: 'AS', state: 'Assam', value: props.StateMapData[27].totalcases },
          { id: 'BR', state: 'Bihar', value: props.StateMapData[21].totalcases },
          { id: 'CT', state: 'Chhattisgarh', value: props.StateMapData[17].totalcases },
          { id: 'GA', state: 'Goa', value: props.StateMapData[24].totalcases },
          { id: 'GJ', state: 'Gujarat', value: props.StateMapData[18].totalcases },
          { id: 'HR', state: 'Haryana', value: props.StateMapData[4].totalcases },
          { id: 'HP', state: 'Himachal Pradesh', value: props.StateMapData[19].totalcases },
          { id: 'JH', state: 'Jharkhand', value: props.StateMapData[26].totalcases },
          { id: 'KA', state: 'Karnataka', value: props.StateMapData[9].totalcases },
          { id: 'KL', state: 'Kerala', value: props.StateMapData[0].totalcases },
          { id: 'MP', state: 'Madhya Pradesh', value: props.StateMapData[20].totalcases },
          { id: 'MH', state: 'Maharashtra', value: props.StateMapData[10].totalcases },
          { id: 'MN', state: 'Manipur', value: props.StateMapData[22].totalcases },
          { id: 'ML', state: 'Meghalaya', value: props.StateMapData[32].totalcases },
          { id: 'MZ', state: 'Mizoram', value: props.StateMapData[23].totalcases },
          { id: 'NL', state: 'Nagaland', value: props.StateMapData[31].totalcases },
          { id: 'OR', state: 'Odisha', value: props.StateMapData[14].totalcases || 0 },
          { id: 'PB', state: 'Punjab', value: props.StateMapData[11].totalcases },
          { id: 'RJ', state: 'Rajasthan', value: props.StateMapData[3].totalcases },
          { id: 'SK', state: 'Sikkim', value: 'NA' },
          { id: 'TN', state: 'Tamil Nadu', value: props.StateMapData[7].totalcases },
          { id: 'TG', state: 'Telangana', value: props.StateMapData[2].totalcases },
          { id: 'TR', state: 'Tripura', value: props.StateMapData[30].totalcases },
          { id: 'UT', state: 'Uttarakhand', value: props.StateMapData[13].totalcases },
          { id: 'UP', state: 'Uttar Pradesh', value: props.StateMapData[5].totalcases },
          { id: 'WB', state: 'West Bengal', value: props.StateMapData[16].totalcases },
          { id: 'AN', state: 'Andaman and Nicobar Islands', value: props.StateMapData[25].totalcases },
          { id: 'CH', state: 'Chandigarh', value: props.StateMapData[17].totalcases },
          { id: 'DN', state: 'Dadra and Nagar Haveli', value: props.StateMapData[29].totalcases },
          { id: 'DD', state: 'Daman and Diu', value: 'NA' },
          { id: 'DL', state: 'Delhi', value: props.StateMapData[1].totalcases },
          { id: 'JK', state: 'Jammu and Kashmir', value: props.StateMapData[8].totalcases },
          { id: 'LA', state: 'Ladakh', value: props.StateMapData[6].totalcases },
          { id: 'LD', state: 'Lakshadweep', value: "NA" },
          { id: 'PY', state: 'Puducherry', value: props.StateMapData[15].totalcases }
        ]
      }
      setData(getHeatMapData2());
    }else{
        console.log('undefined')
    }
  },[props]);

  const gradientData = {
    fromColor: COLOR_RANGE[0],
    toColor: COLOR_RANGE[COLOR_RANGE.length - 1],
    min: 0,
    max: data.reduce((max, item) => (item.value > max ? item.value : max), 0)
  };

  const colorScale = scaleQuantile()
    .domain(data.map(d => d.value))
    .range(COLOR_RANGE);

  const onMouseEnter = (geo, current = { value: 'NA' }) => {
    return () => {
      setTooltipContent(`${geo.properties.name}: ${current.value}`);
    };
  };

  const onMouseLeave = () => {
    setTooltipContent('');
  };

  const onChangeButtonClick = () => {
    setData(getHeatMapData());
  };

  return (
    <div className="full-width-height container">
      <ReactTooltip>{tooltipContent}</ReactTooltip>
        <ComposableMap
          projectionConfig={PROJECTION_CONFIG}
          projection="geoMercator"
          width={250}
          height={190}
          data-tip=""
        >
          <Geographies geography={INDIA_TOPO_JSON}>
            {({ geographies }) =>
              geographies.map(geo => {
                const current = data.find(s => s.id === geo.id);
                return (
                  <Geography
                    key={geo.rsmKey}
                    geography={geo}
                    fill={current ? colorScale(current.value) : DEFAULT_COLOR}
                    style={geographyStyle}
                    onMouseEnter={onMouseEnter(geo, current)}
                    onMouseLeave={onMouseLeave}
                  />
                );
              })
            }
          </Geographies>
        </ComposableMap>
    </div>
  );
}

export default App;
