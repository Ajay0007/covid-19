import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import './GlobalStat.css';
import ReactCountryFlag from "react-country-flag";
import CountUp from 'react-countup';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';




class GlobalStat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModelVisible: false,
            WorldCountries:[],
            WorldCountriesCopy:[]
        }
    }
    componentDidMount(){
        this.getWorldCountryCases()
    }
    componentWillReceiveProps(nextProp) {
        if (nextProp.isCountryModalVisible) {
            this.setState({ isModelVisible: nextProp.isCountryModalVisible })
        } else {
            this.setState({ isModelVisible: nextProp.isCountryModalVisible })
        }
    }
    getWorldCountryCases(){
        fetch('https://corona.lmao.ninja/v2/countries?sort=country')
        .then(res => res.json())
        .then((data) =>{
            this.setState({ WorldCountries: data})
            this.setState({ WorldCountriesCopy: data})
        }).catch((err) =>{
            console.error('Global Countries Records',err)
        })
    }
    handleCountrySearch(e){
        if(e.target.value !== ''){
            let searchedResult = this.state.WorldCountries.filter((item) =>{
                return item.country.toLowerCase().includes(e.target.value.toLowerCase())
            })
            this.setState({ WorldCountries:searchedResult})
        }else{
            this.setState({ WorldCountries:this.state.WorldCountriesCopy})
        }
    }
    render() {
        return (
            <React.Fragment>
                <div className={(this.state.isModelVisible) ? 'CountriesModal ShowModal' : 'CountriesModal HideModal'}>
                    <Grid container
                        justify="space-evenly"
                        alignItems="center"
                    >
                            <Paper component="form" className='root'>
                                <InputBase
                                    onChange={this.handleCountrySearch.bind(this)}
                                    className='input'
                                    placeholder="Search Countries..."
                                    inputProps={{ 'aria-label': 'Search Countries...' }}
                                />
                                <IconButton className='iconButton' aria-label="search">
                                    <SearchIcon />
                                </IconButton>
                                <Divider className='divider' orientation="vertical" />
                            </Paper>
                        <Grid item xs={12} md={12}>
                        <TableContainer component={Paper} >
                            <Table className='table' aria-label="simple table">
                                <TableHead>
                                <TableRow xs={12}>
                                    <TableCell style={{'fontFamily': 'archia'}} className='TableHead'>Sr.No</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='TableHead'>Countries</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='TableHead'>Name</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='TableHead countryTodaycases'>TodayCases</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='TableHead countryTodaydeathth'>TodayDeaths</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='TableHead countryConfirmed'>Confirmed</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='TableHead countryActive'>Active</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} style={{'fontFamily': 'archia'}}  className='TableHead countryRecovered'>Recovered</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='TableHead'>Dead</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                {this.state.WorldCountries.map((eachCountry, index) => (
                                    <TableRow key={index}>
                                    <TableCell style={{'fontFamily': 'archia'}} className='countryNames'>{index + 1}</TableCell>
                                    <TableCell component="th" scope="row">
                                    { (!!eachCountry.countryInfo.iso2)?
                                    <ReactCountryFlag
                                        countryCode={eachCountry.countryInfo.iso2 || ''}
                                        svg
                                        style={{
                                            width: '6em',
                                            height: '1.8em',
                                        }}
                                    /> : <span className='Notavailable'>Not Available</span>
                                    } 
                                    </TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='countryNames'>{eachCountry.country}</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='countryTodaycase        textCenter'>{eachCountry.todayCases}</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='countryTodaydeath textCenter'>{eachCountry.todayDeaths}</TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='confirmed textCenter'><CountUp end={eachCountry.cases} duration={3.75} separator=","></CountUp></TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='active textCenter'><CountUp end={eachCountry.active} duration={3.75} separator=","></CountUp></TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='recovered textCenter'><CountUp end={eachCountry.recovered} duration={3.75} separator=","></CountUp></TableCell>
                                    <TableCell style={{'fontFamily': 'archia'}} className='dead textCenter'><CountUp end={eachCountry.deaths} duration={3.75} separator=","></CountUp></TableCell>
                                    </TableRow>
                                ))}
                                </TableBody>
                            </Table>
                            </TableContainer>
                        </Grid>
                    </Grid>
                </div>
            </React.Fragment>
        );
    }
}

export default GlobalStat;