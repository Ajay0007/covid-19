import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';


class WorldMap extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isModelVisible: false,
         }
    }
    componentWillReceiveProps(nextProp){
        if (nextProp.isMapModalVisible) {
            this.setState({ isModelVisible: nextProp.isMapModalVisible })
        } else {
            this.setState({ isModelVisible: nextProp.isMapModalVisible })
        }
    }
    render() { 
        return ( 
            <React.Fragment>
                <div className={(this.state.isModelVisible) ? 'CountriesModal ShowModal' : 'CountriesModal HideModal'}>
                    <Grid container
                    justify="space-evenly"
                    alignItems="center"
                    >
                        <Grid item xs={12} sm={12} md={11} lg={11} xl={11} >
                            <Card variant="outlined">
                                <CardContent>
                                   WorldMap
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </div>
            </React.Fragment>
         );
    }
}
 
export default WorldMap;
