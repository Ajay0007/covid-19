import React from 'react';
import './App.css';
import NavBar  from '../src/Components/NavBar/NavBar.js';


function App() {
  return (
    <div className="App">
      <NavBar />
    </div>
  );
}

export default App;
